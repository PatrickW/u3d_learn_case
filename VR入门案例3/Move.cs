using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // 可改变速度
    public float speed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // 函数内的每一帧代码，都会被执行
    void Update()
    {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        transform.Translate(x,0,z);
        //transform.Translate(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
        
    }
}

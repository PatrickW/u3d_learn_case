using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sound : MonoBehaviour
{
    public Slider musicslider;
    public Toggle Tog;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().enabled = true;
        GetComponent<AudioSource>().Play();
    }

    // 控制播放/停止
    public void Music(){
        if(Tog.isOn == false){
            GetComponent<AudioSource>().enabled = false;
            GetComponent<AudioSource>().Stop();
        }else{
            GetComponent<AudioSource>().enabled = true;
            GetComponent<AudioSource>().Play();
        }
    }

    // 设置音量
    public void MusicVolume(){
         GetComponent<AudioSource>().volume = musicslider.value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
